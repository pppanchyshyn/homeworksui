 //Написати функцію, яка буде виводити символи (включаючи пробыли, цифри) які повторюются в строці

 function remember(str) {
     var res = [];
     for (var i = 0; i < str.length; i++) {
         for (var j = i + 1; j < str.length; j++) {
             if (str.charAt(i) === str.charAt(j)) {
                 if (!res.includes(str.charAt(i))) {
                     res.push(str.charAt(i));
                     break;
                 }
             }
         }
     }
     return res;
 }

 console.log(remember("apple"));
 console.log(remember("apPle"));
 console.log(remember("pippi"));
 console.log(remember('Pippi'));
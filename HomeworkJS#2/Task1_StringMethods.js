 // 1. Написати функцію, скорочує строку і додає три крапки 

 function truncate(str, maxlength) {
     return str.substr(0, maxlength).concat('...');
 }

 console.log(truncate('asdsfwsa', 3));

 // 2. Знайти кількість букв "а" в речені "Назва Львів дана місту на честь князя Лева Даниловича, сина засновника Львова Данила Галицького." використовуючи indexOf

 function findLetterCount(str, letter) {
     var count = 0;
     for (var i = 0; i < str.length; i++) {
         if (str.charAt(i) == letter) {
             count++;
         }
     }
     return count;
 }

 var str1 = 'Назва Львів дана місту на честь князя Лева Даниловича, сина засновника Львова Данила Галицького.';
 console.log(findLetterCount(str1, 'а'));

 // 3. Написати функцію, яка буде фільтрувати рядок і видиляти слова "xxxx" i "polityka"

 function deleteXXX(str) {
     var res;
     var c = '[censored]'
     res = str.replace(/xxxx/g, c);
     res = res.replace(/politika/g, c);
     return res
 }

 var str2 = 'free xxxx and politika without sms and registration';
 console.log(deleteXXX(str2))

 // 4. Профільтрувати строку "іва 24 уц ац34434ауку" і виветси тільки цифри

 function findAllNumbers(str) {
     var r = /\d+/g;
     var matches = str.match(r);
     return matches.join('');
 }

 var str3 = 'іва 24 уц ац34434ауку';
 console.log(findAllNumbers(str3));
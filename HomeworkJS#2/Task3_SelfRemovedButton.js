// Create button “Remove Me” that remove itself

var body = document.querySelector('body');
var button = document.createElement('button');
button.textContent = 'Remove Me';
body.appendChild(button);

function eventHandler(e) {
    body.removeChild(button);
}

button.addEventListener('click', eventHandler)
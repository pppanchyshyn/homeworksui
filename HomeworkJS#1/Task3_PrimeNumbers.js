// Task 3
// Function using loop for found all prime numbers

function findPrimeNumbers(min, max) {
    var i;
    var primeNumbers = [];
    while (min <= max) {
        var isPrime = true;
        for (i = min - 1; i > 1; i--) {
            if (min % i === 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            primeNumbers.push(min);
        }
        min++
    }
    return primeNumbers;
}

alert(findPrimeNumbers(1, 10));
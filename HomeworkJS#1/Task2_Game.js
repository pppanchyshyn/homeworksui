// task 2
// Write game
// Random generate number from 1 to 5
// 1. Ask if user want to play by confirm(); If user cancel - show by alert ('Not today').
// 2. User write number using prompt();
// 3. User have 3 turn to win.
// 4. User choose correct number - show alert with 'Congratulations! You are winner!'
// 5. If user chooose wrong number by 3 times - show 'Today is not your day.'

let magicNumber = 1 + Math.round(Math.random() * 4);
console.log(magicNumber)
let attemptsNumber = 3;
let letsPlay = confirm("Hello, User, I want to play a game");
if (!letsPlay) {
    alert("Not today")
}
while (attemptsNumber > 0) {
    let userAnswer = parseInt(prompt("Guess the number, you have " + attemptsNumber + " attempts:"));
    if (userAnswer == magicNumber) {
        alert("Congratulations! You are winner! P.S: Today")
        break;
    } else {
        attemptsNumber--;
    }
}
alert("Today is not your day.");